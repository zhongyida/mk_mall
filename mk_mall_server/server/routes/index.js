var express = require('express');
var router = express.Router();

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var conn = mongoose.connect('mongodb://127.0.0.1:27017/local');
var User = new mongoose.Schema({
  name: String,
  email: String,
  age: String
});

// var myModel = User.model('user', User);

/* GET index listing. */
router.get('/', function (req, res, next) {
  User.findOne({ name: "zhangangs" }, function (err, user) {
    console.log(user);
    res.render('hello', { title: 'Express', user: user });
  });
});


module.exports = router;
