var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    "userId": String,
    "userName": String,
    "userPwd": String,
    "orderList": Array,
    "cartList": [
        {
            "productid": String,
            "productName": String,
            "salePrice": Number,
            "productImage": String,
            "checked": String,
            "productNum": Number
        }
    ],
    "addressList": [
        {
            "addressId": String,
            "userName": String,
            "streetName": String,
            "postCode": Number,
            "tel": Number,
            "isDefault": Boolean
        }
    ]
});

module.exports = mongoose.model("User", userSchema);
// 这里的model([modelName], [schemaName], [databaseName]); 如果databaseName没有,则默认在modelName后加入s的集合中
