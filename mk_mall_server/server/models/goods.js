var mongoose = require('mongoose')
// var Schema = mongoose.Schema;

var produtSchema = new mongoose.Schema({
    "productid": String,
    "productName": String,
    "salePrice": Number,
    "checked": String,
    "productNum": Number,
    "productImage": String
});

module.exports = mongoose.model('Good', produtSchema);
