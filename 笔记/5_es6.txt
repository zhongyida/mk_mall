es6 的创建目标是用来编写复杂的大型应用程序，成为企业级开发语言

es6常用命令

1 函数的Rest参数和扩展
2 promise使用
3 module.exports和ES6 import/export的使用

--------Rest参数------
function sum(x,y,z) {
    let total = 0;
    if (x)total += x;
    if (y) total += y;
    if (z) total += z;
    console.log(`total1:${total}`);        
}

function sum2(...m) {
    let total = 0;
    for(var i of m){
        console.log(m);
        total += i;
    }
    console.log(`total2:${total}`);
}
sum2(4,8,9,10);

let sum3 = (...m) => {
    let total = 0;
    for (var i of m) {
        console.log(m)
        total += i
    }
    console.log(`total3:${total}`);
}
sum3(5,9,6,4);

var [x,y] = [4,8];
console.log(...[4,8]);  // 4 8   拆解数组

let arr1 = [1,3];let arr2 = [5,4];  
console.log([...arr1,...arr2]); //合并数组

var [x,y] = [4,8];      //x = 4   y = 8;
var [x, ...y] = [4, 8,10,36];   //x = 4   y = [8,10,36]

let [a,b,c] = "ES6";   //a = E ;b = S ;c = 6;
let xy = [...'ES6']     //xy = ["E", "S", "6"]


------promise------
解决的问题是callback函数混乱

 let checkLogin = function () {
       return new Promise(function (resolve,reject) {     //resolve是接口成功后的回调函数,reject是接口调用失败后的回调
            // let flag = document.cookie.indexOf("userId") > -1 ? true : false;
            if(flag = true){
                resolve({
                    status:0,
                    result:true
                })
            }else{
                reject("error");
            }
        })
    }
   
    let getUserInfo = () =>{
        return new Promise((resolve,reject)=>{
            let userInfo = {
                userId : "101"
            }
            resolve(userInfo);
        })
    }

    checkLogin().then(function (res) {
        if (res.status == 0) {
            console.log("login success");
            return getUserInfo()
        }
    }).catch((error) => {
        console.log(`error:${error}`);
    }).then((res2) => {
        console.log(`userId:${res2.userId}`);

    })

    Promise.all([checkLogin(),getUserInfo()]).then(([res1,res2]) => {
        console.log(`result1:${res1.result},result2:${res2.userId}`);
    });

    //login success
    //result1:true,result2:101
    //userId:101
---------------import 和 export-----------------------------
exprot一旦暴露的有名字，import中得用{name}来接受
如: 
a.js
export let sum = function(){};
export let minus = function(){};
b.js
import {sum,minus} form './util'  或者用  import * as util form './util'

improt可以异步加载，防止被打包，类似require加载文件


--------------AMD、CMD、Commonjs和ES6对比----------------------
什么是AMD、CMD、COmmonjs
它们之间有什么区别？
项目中都如何运用

AMD是Requirejs在推广中对模块定义的规范化产出
    依赖前置，依赖模块都定义的数组中            [异步]
CMD是Seajs在推广过程中对模块定义的规范化产出
    依赖就近，什么地方使用依赖，就在什么地方应用  [同步]
commonjs规范 - module.exports
    nodejs后端使用，前端不支持
    
ES6 export/import