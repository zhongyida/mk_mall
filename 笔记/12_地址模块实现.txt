地址列表功能的实现

地址切换和展开功能实现

默认地址

删除地址


mongodb在数据插入的时候自带一个唯一的ID，这将方便我们的查询，但是因为这个ID是特殊的Objectid 类型，所以我们在使用ID进行查询的时候要把我们取到的字符串类型的id转换成ObjectId类型

下面是方法:

var mongoose = require('mongoose');
var id = mongoose.Types.ObjectId('576cd26698785e4913c5d0e2');

这样就能把我们取到的string类型的ID转换为object用来查询了
需要注意的是，在查询时候字段应该为
{"_id":id}