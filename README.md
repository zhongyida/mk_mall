# 商城网站

#### 项目介绍
本网站是参考学习慕课网课程学习结果，学习地址https://coding.imooc.com/class/113.html。
前端框架使用vue.js
后端是nodejs,框架使用是express
数据库是使用MongoDB

中间修复了视频中多个缺陷问题，也写了一些方便操作的脚本文件。


#### 软件架构
VUE+nodejs+MongoDB



#### 安装教程
1 进入mk_mall中 运行 npm install
2 安装完成后，执行start_portal.sh文件或者执行 npm run dev
3 进入mk_mall_server中 运行 npm install
4 安装MongoDB并启动
5 进入mk_mall_server中执行DB_import.sh文件，导入项目中的基础数据，数据文件在db文件夹中，项目运行后自己的数据可以通过运行DB_export.sh文件导出在此目录中
6 执行Server_start.sh文件，启动Web服务


#### 使用说明

在mk_mall_server下，是服务端的代码
1. DB_import.sh DB_export.sh 是数据库的数据导入和导出的脚本文件，项目初始化，启动mongo后可使用，mk_mall_server中的db文件夹是数据库数据文件。(注意：先得启动MongoDB才能运行成功)
2. DB_start       是启动MongDB的脚本文件
3. Server_start.sh    是后端服务启动文件

在mk_mall下，是前端代码
1 start_portal.sh    是启动前端服务的脚本


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)