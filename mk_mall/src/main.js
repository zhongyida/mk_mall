// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import axios from 'axios'
import VueLazyLoad from 'vue-lazyload'
import VueInfiniteScroll from 'vue-infinite-scroll'

import './assets/base.css'
import './assets/checkout.css'
import './assets/product.css'
import store from './store/store'
Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueLazyLoad, {
  loading:'/assets/loading-svg/loading-bars.svg',
  try: 3 // default 1
})
Vue.use(VueInfiniteScroll);
/* eslint-disable no-new */

// const store = new Vuex.Store({
//   state: {
//     nickName: '',
//     cartCount: 0
//   },
//   mutations: {
//     //更新用户信息
//     updateUserInfo(state, nickName) {
//       state.nickName = nickName;
//     },
//     updateCartCount(state, cartCount) {
//       state.cartCount += cartCount;
//     }
//   }
// });
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
