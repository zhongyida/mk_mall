import Vue from 'vue' // 引入vue
import Vuex from 'vuex' // 引入vuex
Vue.use(Vuex) // 注册vuex
export default new Vuex.Store({ // 导出并new vuex
  state: { // 相当于vue里的data 存放整个项目的各个状态
    nickName: '',
    cartCount: 0
  },
  mutations: {
    initData (state, data) {
      for (var key in data) {
        state[key] = data[key]
      }
    },
    // 更新用户信息
    updateUserInfo (state, nickName) {
      state.nickName = nickName
    },
    updateCartCount (state, cartCount) {
      state.cartCount += cartCount
    },
    clearCartCount (state, cartCount) {
      state.cartCount = cartCount
    }
  }
})
