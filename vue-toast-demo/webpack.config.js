var path = require('path')
var VueLoaderPlugin = require('vue-loader/lib/plugin');
module.exports = {
    entry :"./src/lib/index.js",
    output:{
        path: path.join(__dirname, './dist'),
        filename:'vue-toast-demo.js',
        libraryTarget:"umd",
        library:'VueToastDemo'
    },

    module:{
        rules:[
            {
                test:/\.vue$/,
                loader:'vue-loader',
                exclude: /node-modules/,
                options:{
                    loaders:{
                        scss:'style-loader!css-loader!sass-loader'
                    }
                }
            },{
                test: /\.js$/,
                loader: 'babel-loader',
                include:path.join(__dirname,'src'),
                exclude:/node-modules/
            }
        ]
    },
    plugins:[
        new VueLoaderPlugin()
    ]

}